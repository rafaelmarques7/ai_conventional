import sys
import functions
import pandas as pd
from models import ProjectModel
from search_algorithms import GraphSearch
from strategies import BreadthFirst, DeapthFirst, UniformCost

if __name__ == "__main__":
    args = sys.argv[1:]
    (search_type, filename) = functions.checkInputArgs(args)
    data = functions.readData(filename)
    problem = ProjectModel(data)
    strategy = UniformCost()
    GraphSearch(problem, strategy)
