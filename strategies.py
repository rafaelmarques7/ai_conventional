class BreadthFirst:
    def select(self, frontier):
        return frontier.pop(0)

    def expand(self, node_parent, problem):
        nodes_child = []
        for n, action in enumerate(problem.actions):
            state_parent = node_parent['state']
            state_new = problem.applyAction(state_parent, action)
            nodes_child.append({
                'parent': node_parent['id'],
                'action_executed': action,
                'id': node_parent['id'] + n + 1,
                'state': state_new,
                'cost': node_parent['cost'] + 1
            })
        return nodes_child

class DeapthFirst:
    def select(self, frontier):
        return frontier.pop()

    def expand(self, node_parent, problem):
        nodes_child = []
        for n, action in enumerate(problem.actions):
            state_parent = node_parent['state']
            state_new = problem.applyAction(state_parent, action)
            nodes_child.append({
                'parent': node_parent['id'],
                'action_executed': action,
                'id': node_parent['id'] + n + 1,
                'state': state_new,
                'cost': node_parent['cost'] + 1
            })
        return nodes_child

class UniformCost:
    #NOTE:
    #in case of state repetition (child state is already in frontier)
    #the path costs should be compared, and keep only the CHEAPER
    def select(self, frontier):
        #reorder the list according to cost
        frontier.sort(key = lambda x: x['cost'])
        return frontier.pop(0)

    def expand(self, node_parent, problem, frontier, explored):
        nodes_child = []
        for n, action in enumerate(problem.getActions(node_parent['state'])):
            print('selected action: vertice, launch_id ', action[0], action[1]['id'])
            state_parent = node_parent['state']
            state_new = problem.applyAction(state_parent, action)
            if state_new != state_parent:
                nodes_child.append({
                    'parent': node_parent['id'],
                    'action_executed': (action[0], action[1]['id']),
                    'id': node_parent['id'] + n + 1,
                    'state': state_new,
                    'cost': problem.getCost(state_new)
                })
        frontier, explored = self.updateLists(nodes_child, frontier, explored)
        return frontier, explored


    def updateLists(self, nodes_child, frontier, explored):
        check_list = frontier + explored
        for n in check_list:
            for nn in nodes_child:
                if nn['state'] == n['state']:
                    #check cost
                    if nn['cost'] < n['cost']:
                        index = frontier.index(n)
                        frontier[index] = nn
                        nodes_child.remove(nn)
                    else:
                        nodes_child.remove(nn)
        for nn in nodes_child:
            frontier.append(nn)
        return frontier, explored
