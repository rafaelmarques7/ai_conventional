from copy import deepcopy
import functions

class ProjectModel:
    def __init__(self, data):
        self.edges = data['edges']
        self.vertex = data['vertices']
        self.launches = data['launches']
        self.state_goal = self.getStateGoal()
        self.state_init = self.getStateInit()
        self.connections = functions.createConnections(self.vertex, self.edges)
        self.displayVariables()

    def displayVariables(self):
        print('vertex', self.vertex)
        print('edges', self.edges)
        print('launches', self.launches)

    def getStateGoal(self):
        return [vertice_name for vertice_name, vertice_weight in self.vertex.items()]

    def checkGoal(self, node, explored):
        state, assigned_vertex = node['state'], []
        for launch_ID, vertex_on_launch in state.items():
            for vertex in vertex_on_launch:
                assigned_vertex.append(vertex)
        goal_achieved = set(assigned_vertex) == set(self.state_goal)
        if goal_achieved:
            self.displaySolutionAndExit(node, explored)

    def displaySolutionAndExit(self, node_sel, explored):
        solution, node = [], node_sel
        while True:
            solution.append(node)
            id_parent = node['parent']
            if id_parent == 0:
                break
            for n in explored:
                if n['id'] == id_parent:
                    node = n
                    break
        solution = sorted(solution, key = lambda x: x['parent'])
        print('____________________________________________________________________________________________________________________')
        print('\n               SOLUTION:\n')
        for item in solution:
            print(item)
        print('Solution cost: ')
        print(node_sel['cost'])
        print('____________________________________________________________________________________________________________________')
        exit()

    def getStateInit(self):
        state_init = {}
        for l in self.launches:
            state_init[l['id']] = []
        return state_init

    def getActions(self, state):
        vertex_missing = set(self.getAllVertex()) - set(self.getAllAssignedVertex(state))
        return [(vertex, launch) for vertex in vertex_missing for launch in self.launches]

    def applyAction(self, state, action):
        #this function has to respect the constrains of the problem
        #if the constrains can not be respected, it should return the original state
        #actions = tuple(vertice, launch)
        if not self.canApplyAction(state, action):
            return state
        vertice, launch = action[0], action[1]
        new_state = deepcopy(state)
        new_state[launch['id']].append(vertice)
        return new_state

    def canApplyAction(self, state, action):
        return self.payloadRespected(state, action) and self.connectionsRespected(state, action)

    def payloadRespected(self, state, action):
        print('action - vertex, launch_id: ', action[0], action[1]['id'])
        vertice, launch = action[0], action[1]
        curr_weight_launch = self.getWeight(state, launch)
        max_payload_launch = self.launches[launch['id']-1]['payload']
        vertice_weight = self.vertex[vertice]
        return vertice_weight + curr_weight_launch <= max_payload_launch

    def getWeight(self, state, launch):
        weight = 0
        for vertex in state[launch['id']]:
            weight += self.vertex[vertex]
        return weight

    def connectionsRespected(self, state, action):
        #if state is empty, can add to any launch
        if state == self.getStateInit():
            return True
        sel_vertex, sel_launch_id = action[0], action[1]['id']
        sel_vertex_connections = self.connections[sel_vertex]
        assigned_vertex = self.getPrevAssignedVertex(state, sel_launch_id)
        for av in assigned_vertex:
            for cv in sel_vertex_connections:
                if av == cv:
                    print('sel_vertex: ', sel_vertex)
                    print('sel_vertex_connections:', sel_vertex_connections)
                    print('assigned_vertex', assigned_vertex)
                    return True
        return False

    def getPrevAssignedVertex(self, state, ID):
        #return all vertex in previous and current launch
        return [vertex for launch_id, vertex_on_launch in state.items() for vertex in vertex_on_launch if int(launch_id) <= int(ID)]
        """
        prev_and_current_ids = [i for i in range(1, launch_id + 1)]
        assigned_vertex = []
        for ID in prev_and_current_ids:
            for v in state[ID]:
                assigned_vertex.append(v)
        return assigned_vertex
        """

    def getAllAssignedVertex(self, state):
        return [vertex for launch_id, vertex_on_launch in state.items() for vertex in vertex_on_launch]
        """
        vertex_assigned = []
        for launch_id, vertex_on_launch in state.items():
            for vertex in vertex_on_launch:
                vertex_assigned.append(vertex)
        return vertex_assigned
        """

    def getAllVertex(self):
        return [vertex_name for vertex_name, vertex_weigth in self.vertex.items()]

    def getCost(self, state):
        cost = 0
        for launch_id, vertex_list in state.items():
            if vertex_list != []:
                cost += self.launches[launch_id-1]['fixed_cost']
                for v in vertex_list:
                    cost += self.vertex[v]*self.launches[launch_id-1]['var_cost']
        return cost
