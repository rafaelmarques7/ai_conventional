import os
import copy
from itertools import combinations

def getDirFiles():
    fnames = []
    for root, dirs, files in os.walk('.'):
        for name in files:
            fnames.append(name)
    return fnames

def checkInputArgs(args):
    search_type, filename = None, None
    if len(args) != 2:
        print('error in args size\nEXITING')
        exit()
    if args[0] != '-u' and args[0] != '-i':
        print('error in search algorithm type\nEXITING')
        exit()
    search_type = args[0]
    if args[1] not in getDirFiles():
        print('error in file name \nEXITING')
        exit()
    filename = args[1]
    print("arguments passed are corrected. will work now\n")
    return (search_type, filename)

def readData(filename):
    with open(filename, 'r') as f:
        data = translateData(f)
    return data

def translateData(l):
    launches, vertices, edges = [], {}, []
    for line in l:
        if line[0] == 'V':
            vert_data = line.split()
            vert_name = vert_data[0]
            vert_weight = float(vert_data[1])
            vertices[vert_name] = vert_weight
        if line[0] == 'E':
            edge_data = line.split()
            edges.append((edge_data[1], edge_data[2]))
        if line[0] == 'L':
            _, date, payload, fixed_cost, var_cost = line.split()
            launches.append({
                'date': date,
                'payload': float(payload),
                'fixed_cost': float(fixed_cost),
                'var_cost': float(var_cost),
            })
    #order launches by date
    launches = sorted(launches, key = lambda x: x['date'])
    for n, launch in enumerate(launches):
        launches[n]['id'] = n + 1
    return {
        'launches': launches,
        'vertices': vertices,
        'edges': edges
    }

"""
def createConnections(vertices, edges):
    sets = []
    edges_connected = []
    for v in vertices:
        v_connections = []
        for pair in edges:
            if pair[0] == v:
                v_connections.append(pair[1])
            elif pair[1] == v:
                v_connections.append(pair[0])
        edges_connected.append({v: v_connections})
    return edges_connected
"""

def createConnections(vertices, edges):
    sets = []
    edges_connected = {}
    for v in vertices:
        v_connections = []
        for pair in edges:
            if pair[0] == v:
                v_connections.append(pair[1])
            elif pair[1] == v:
                v_connections.append(pair[0])
        edges_connected[v] =  v_connections
    return edges_connected

def createSets(vertices, edges):
    #[{'V1': ['V2']}, {'V3': ['V2']}, {'V2': ['V1', 'V3']}]
    edges_connected = createConnections(vertices, edges)
    sets_possible = []
    for connect_dict in edges_connected:
        vertice_sets = []
        for vertice, connecting_vertices in connect_dict.items():
            #set of just one component
            set0 = (vertice, )
            vertice_sets.append(set0)
            #set of component + all sets of n elements, n <= len(connecting_vertices)
            for i in range(1, len(connecting_vertices)+1):
                comb = [x for x in combinations(connecting_vertices, i)]
                for comb_i in comb:
                    vertice_sets.append((vertice, ) + comb_i)
        sets_possible.append(vertice_sets)
    print('sets POSSIBLE\n')
    for item in sets_possible:
        print(item)
