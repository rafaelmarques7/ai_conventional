#a Graph is a search space in which nodes CAN be repeated
from strategies import BreadthFirst, DeapthFirst, UniformCost

def GraphSearch(problem, strategy):
    "returns solution or failure"
    frontier, explored, iter_num = initialise(problem), [], 0

    while True:
        iter_num += 1
        displayStatus(iter_num, frontier, explored)

        #check if frontier is empty and return failure if it is.
        if not frontier:
            failure()

        #select a node (removal from frontier is strategy internal proccess)
        node_sel = strategy.select(frontier)

        #check for goal state and return solution if goal state
        problem.checkGoal(node_sel, explored)

        #update explored list with currently selected node
        explored.append(node_sel)

        #expand the nodes and add to frontier
        applyExpandStrategy(node_sel, frontier, explored, strategy, problem)

def initialise(problem):
    "initialise the frontier with the initial state of the problem"
    node_init = {
        'parent': 0,
        'id': 1,
        'action': None,
        'state': problem.state_init,
        'cost': 0
    }
    return [node_init]

def applyExpandStrategy(node_sel, frontier, explored, strategy, problem):
    "aplies the selected strategy to expand the frontier based on the selected node"
    #if node_sel in explored:
    #    return None
    if strategy.__class__.__name__ == 'UniformCost':
        frontier, explored = strategy.expand(node_sel, problem, frontier, explored)
    else:
        nodes_new = strategy.expand(node_sel, problem)
        check_list = frontier + explored
        #check if state of each child node is in any of the two lists (open and explored)
        #if yes: remove child node from list
        #in the end, add the remaining child nodes to frontier
        for n in check_list:
            for nn in nodes_new:
                if nn['state'] == n['state']:
                    nodes_new.remove(nn)
        for n in nodes_new:
            frontier.append(n)

def failure():
    print('____________________________________________________________________________________________________________________')
    print('                                     FAILURE. FRONTIER EMPTY. EXITING')
    print('____________________________________________________________________________________________________________________')
    exit()

def displayStatus(iter_num, frontier, explored):
    print('____________________________________________________________________________________________________________________')
    print('iteration: ', iter_num)
    print('frontier size: ', len(frontier))
    print('explored size: ', len(explored))
